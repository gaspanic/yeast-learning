import os
import pathlib
import csv
import imageio
import numpy as np
import matplotlib.pyplot as plt
import time
from skimage import transform
from skimage.morphology import binary_dilation, square, label, binary_erosion, ball
from skimage.segmentation import flood_fill
from scipy.ndimage.morphology import binary_fill_holes
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter
import random as rd
import pickle
import PIL as pil
from keras.models import *
from keras.layers import *
from keras.optimizers import *
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as keras

class CropObject:
  '''This class represents single objects cropped from larger images.'''
  def __init__(self, img, crop, mask, tag, label=None):
    self.img = img
    self.crop = crop
    self.mask = mask
    self.tag = tag
    self.label = label

  def copyCropObject(self):
    return CropObject(self.img, self.crop, self.mask, self.tag, self.label)

  def getTransform(self):
    cropCopy = self.copyCropObject()
    cropCopy.rotateCropObject()
    cropCopy.flipCropObject()
    cropCopy.tposeCropObject()
    cropCopy.elasticTransformCropObject()
    
    return cropCopy  

  def rotateCropObject(self):
    random_degree = rd.uniform(0, 360)
    self.img = transform.rotate(self.img, random_degree, preserve_range=False, mode='constant', cval=0)
    self.mask = np.round(transform.rotate(self.mask, random_degree, preserve_range=False, mode='constant', cval=0), 0)
    self.crop = np.round(transform.rotate(self.crop, random_degree, preserve_range=False, mode='constant', cval=0), 0)

  def flipCropObject(self):
    axes = list(np.sort(rd.sample([0,1],rd.randint(0,2))))
    self.img = np.flip(self.img, axis=axes)
    self.mask = np.flip(self.mask, axis=axes)
    self.crop = np.flip(self.crop, axis=axes)

  def tposeCropObject(self):
    random_tpose = rd.randint(0,1)
    self.img = np.moveaxis(self.img,0,random_tpose)
    self.mask = np.moveaxis(self.mask,0,random_tpose)
    self.crop = np.moveaxis(self.crop,0,random_tpose)

  def elasticTransformCropObject(self, random_state=None):
    '''Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
      Convolutional Neural Networks applied to Visual Document Analysis", in
      Proc. of the International Conference on Document Analysis and
      Recognition, 2003.'''

    assert len(self.img.shape)==2

    alpha = rd.randint(0,5)
    sigma = rd.randint(0,3)+2

    if random_state is None:
        random_state = np.random.RandomState(None)

    shape = self.img.shape

    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha

    x, y = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), indexing='ij')
    indices = np.reshape(x+dx, (-1, 1)), np.reshape(y+dy, (-1, 1))

    self.img = map_coordinates(self.img, indices, order=1).reshape(shape)

  def getCropObjectMetrics(self):
    idx = np.where(self.crop>0) #All coordinates
    dim = np.max(idx, axis=1)-(np.min(idx, axis=1)-1) #Width and height
    pos = np.min(idx, axis=1) #Bottom left coordinates

    return idx, dim, pos


class RawImageObject:
  def __init__(self, img, mask, labels):
    self.img = img
    self.mask = mask
    self.labels = labels
    self.cropCollection = []

  def labelMask(self, tags):
    tag_mask = np.copy(self.mask)

    # Clear all unclassified objects and color classify objects in mask
    for i_tag in tags:
      if tag_mask[i_tag[1], i_tag[2]]>0:
        tag_mask = flood_fill(tag_mask, (i_tag[1], i_tag[2]), i_tag[0]+1)
    tag_mask = tag_mask-1
    tag_mask[tag_mask<1] = 0
    self.tag = np.max(tag_mask)
    #self.mask = (tag_mask>0)
    self.mask = tag_mask
  
  def enumerateMask(self):
    # Enumerate objects
    id_mask = (self.mask > 0)
    id_mask = label(id_mask)
    self.idmask = id_mask

  def cropImage(self, cropSize):
    object_ids = np.unique(self.idmask)[1:]

    for i_id in object_ids:
      x_idx, y_idx = np.where(self.idmask == i_id)
      
      x = int(np.mean((np.max(x_idx),(np.min(x_idx)))))
      x0 = max(min(x+int(cropSize/2),self.idmask.shape[0])-cropSize,0)
      
      y = int(np.mean((np.max(y_idx),(np.min(y_idx)))))
      y0 = max(min(y+int(cropSize/2),self.idmask.shape[1])-cropSize,0)

      cel = np.asarray(np.copy(self.img[x0:(x0+cropSize), y0:(y0+cropSize)]), dtype=float)

      msk = np.copy(self.idmask[x0:(x0+cropSize), y0:(y0+cropSize)])
      msk[msk != i_id] = 0
      msk = (msk>0)
      
      crp = np.copy(msk)
      crp = binary_dilation(crp, square(9))
      #crp = binary_dilation(crp, ball(9)) 
      crp = (crp>0)

      tag = np.max(self.mask[x_idx, y_idx])
      label = self.labels[tag-1]

      #Append cropped object to collection
      self.cropCollection.append(CropObject(cel, crp, msk, tag, label))

  def getCropCollection(self):
    return self.cropCollection


class ImageObject:
  def __init__(self, bgList, imgSize):
    self.img, self.mask = self.getBackground(bgList, imgSize)
    self.weightMask = np.copy(self.mask)
    self.bboxes = []

  def addObjectsToImage(self, cropList, maxObjects, fractionEmpty, idx):
    #Create list for randomly choosing max number of objects to add for current
    #image, always within limits defined by maxObjects,
    if maxObjects>1:
      max_objects = [[i]*i for i in range(1,maxObjects,)]
      max_objects = [j for i in max_objects for j in i]
    else:
      max_objects = [1,1]

    #Ensure fractionEmpty empty images
    if rd.randint(0,100) > int(fractionEmpty * 100):
      #Add objects to background image
      for i_obj in range(rd.choice(max_objects)):
        #Draw random class
        random_class = rd.randint(0, len(idx)-1)
        #Draw random object from selected class
        random_cell = rd.choice(idx[random_class])
        #Add object to image object
        self.addCell(cropList[random_cell])

  def addCell(self, cropObject):
    #Transform object and object mask
    transformedCropObject = cropObject.getTransform()

    obj = transformedCropObject.img
    obj_crp = transformedCropObject.crop
    obj_msk = transformedCropObject.mask
    obj_tag = transformedCropObject.tag

    #Get object metrics
    obj_idx, obj_size, obj_pos = transformedCropObject.getCropObjectMetrics()
    fil_idx = np.where(obj_msk>0)
    dil_idx = np.where(binary_dilation(obj_msk))

    overlap = 1
    attempt = 0
    imgSize = self.img.shape
    while overlap==1 and attempt<=5:
      #Choose random position in target image
      target_pos = (rd.randint(0, imgSize[0]-obj_size[0]),
                    rd.randint(0, imgSize[1]-obj_size[1]))
    
      #Calculate object and fillmask indices in target image
      target_idx = tuple([obj_idx[i]-obj_pos[i]+target_pos[i] for i in range(2)])
      fil_target_idx = tuple([fil_idx[i]-obj_pos[i]+target_pos[i] for i in range(2)])
      
      #Calculate dilation mask indices in target image
      dil_target_idx = [dil_idx[i]-obj_pos[i]+target_pos[i] for i in range(2)]

      #Find out-of-bounds indices among dilation mask indices
      oob_idx = []
      for i in dil_target_idx:
        oob_idx.extend([j for j in range(len(i)) if i[j]<0 or i[j]>=imgSize[0]])
      oob_idx.sort()
      
      #Mask out-of-bounds indices from dilation mask indices
      dil_target_idx = np.array(dil_target_idx)
      oob_msk = np.ones(dil_target_idx.shape[1], dtype=bool)
      oob_msk[oob_idx] = False
      dil_target_idx = dil_target_idx[:,oob_msk]
      dil_target_idx = tuple(dil_target_idx)

      #Add only if dilation mask of new object overlaps less than px pixels with
      #previous objects' fill masks
      px = 5
      if np.sum(self.mask[dil_target_idx]>0) < px:
        #Merge new object with target site to simulate transparency
        transparency = .7
        obj[obj_idx] = self.img[target_idx] * (1-transparency) + obj[obj_idx] * transparency

        #Adjust object brightness to better match target site
        obj = obj - (np.mean(obj[obj_idx]) - np.mean(self.img[target_idx]))
        obj[obj<0] = 0

        #Add object to target image
        self.img[target_idx] = obj[obj_idx]
        
        #Add dilated object mask to empty mask and get overlapping pixels
        newObject = np.zeros(self.mask.shape)
        newObject[dil_target_idx] = 1
        self.weightMask[(newObject * self.weightMask > 0)] = 0
        self.weightMask[fil_target_idx] = np.max(self.weightMask)+1

        self.mask[fil_target_idx] = obj_tag #Color object according to class

        #Append bounding box
        bbox = list(np.min(target_idx, axis=1))
        bbox.extend(obj_size)
        self.bboxes.append(bbox)

        overlap = 0
      
      attempt += 1

  def makeWeightMap(self):
    '''Weight mask is created by first creating a background array of all
    background pixels (pixels where no cell mask has been pasted). A new eroded
    array is then created, and the eroded array is used to remove excess
    background pixels from the original background array. This results in an
    "array of background pixels surrounding cells".
    
    Next, a foreground array is created representing all foreground pixels
    (pixels representing cell masks), and is similarly eroded. This eroded
    foreground array is used to remove all "fill pixels" from the original
    weight mask, thus leaving cell contours only.
    
    Shortest distance is then calculated from each background pixel to every
    cell, after which distances to the two nearest cells are kept. If no such
    cells exists, a maximal distance (equal to the image diagonal) is used.
    
    Finally, weight map is calculated according to Ronneberger et al 2015.'''

    [img_h,img_w] = self.weightMask.shape

    ###Create X and Y coordinate matrices
    #Fill each matrix with every pixel's x and y coordinate, respectively
    X = np.asarray([list(range(img_w))]*img_w)
    Y = np.transpose([list(range(img_h))]*img_h)

    #Stack into a coordinate matrix. Each pixel now outputs that pixel's
    #coordinates. Once C is flattened, this will serve as a coordinate map
    C = np.dstack((Y,X))
    C = C.reshape((-1,2))

    #Background mask
    B = (self.weightMask==0)
    #B_eroded = binary_erosion(B, square(5))
    B_eroded = binary_erosion(B)
    B_eroded = binary_erosion(B_eroded)
    B[B_eroded] = 0
    B = B.reshape(-1)

    #Foreground mask
    F = np.copy(self.weightMask)
    F_eroded = (F>0)
    F_eroded = binary_erosion(F_eroded)
    F[F_eroded>0] = 0
    F = F.reshape(-1)

    #Get indices of background pixels
    B_idx, = np.where(B==1) #indices of background pixels

    #Get cell IDs, and create a new distance array D
    n_cel = np.unique(F)[1:]
    D = np.zeros(B.shape + (len(n_cel), ))

    #For each cell in image, get indices of foreground pixels
    for i_cel in n_cel:
      F_idx, = np.where(F==i_cel) #indices of foreground pixels for current cell
      F_coords = C[F_idx]

      #For each origin background pixel (i_bg), get shortest distance to current cell
      for i_bg in B_idx:
        B_coord = C[i_bg]
        V = B_coord - F_coords
        N = np.linalg.norm(V,axis=1)
        D[i_bg,i_cel-1] = np.min(N)

    #Create empty layer(s) of "maximal possible distance" if necessary,
    #(if n_cel<2), and stack layers to D
    add_max = max(2-D.shape[1],0)
    D_max = np.zeros(B.shape+(add_max,))
    D_max[B>0,:] = np.sqrt(img_h**2+img_w**2)
    D = np.hstack((D, D_max))
    #Sort D, and keep the two smallest distances for every background pixel
    D = np.sort(D, axis=1)
    D = D[:,:2]

    #Calculate weight map
    #WeightMap calculated as described by Ronneberger et al (arXiv:1505.04597)
    sigma = 1.5 # Sets distance sensitivity
    w0 = 30 # Sets weight penalty
    weightMap = np.square(np.sum(D, axis=1))/(2*sigma**2)
    weightMap = w0*np.exp(-weightMap)
    weightMap[B==0] = 0
    self.weightMap = weightMap.reshape(self.weightMask.shape)+1

    del self.weightMask

  def getBackground(self, bgList, imgSize):
    #Get random background image
    random_bg = rd.randint(0, len(bgList)-1)
    bg = np.copy(bgList[random_bg])

    #Randomly flip background
    axes = list(np.sort(rd.sample([0,1],rd.randint(0,2))))
    bg = np.flip(bg, axis=axes)

    #Randomly rotate background (CW)
    random_cw = rd.randint(0, 3)
    for i_rot in range(random_cw):
      bg = zip(*bg[::-1])
      bg = np.array([list(elem) for elem in bg])

    #Randomly transpose background
    bg = np.moveaxis(bg,0,rd.randint(0, 1))

    #Crop or expand    
    while imgSize != bg.shape[0]:
      if imgSize<bg.shape[0]:
        xy_crop = (rd.randint(0,bg.shape[0]-imgSize),rd.randint(0,bg.shape[0]-imgSize))
        bg = bg[xy_crop[0]:xy_crop[0]+imgSize,xy_crop[1]:xy_crop[1]+imgSize]
      else:
        bg = np.concatenate((bg,bg[-2::-1,:]),axis=0)
        bg = np.concatenate((bg,bg[:,-2::-1]),axis=1)

    return bg, np.zeros(bg.shape, dtype=int)

    
class ImageSet:
  def __init__(self):
    self.bgList = []
    self.cropList = []
    self.labels = None
    self.imageObjects = [[] for i in range(3)]

    self.trainingSet = None
    self.validationSet = None
    self.testSet = None

    self.model = None

  def loadBgs(self, bgDir):
    fileList = os.listdir(bgDir)
    bgList = [i_file for i_file in fileList if 'bg' in i_file]

    for i_bg in bgList:
      bgImg = imageio.imread(os.path.join(bgDir, i_bg)).T
      self.bgList.append(bgImg)

  def getBgs(self):
    return np.array(self.bgList)

  def cropImages(self, imgDir):
    '''Crops cells from images. First loads tagData from a table stored in
    tag_file.csv in the passed directory. Then images and corresponding masks
    are found and imported via the "img" and "msk" pattern, respectively.
    Info from tagData is then used to locate, crop, and tag each object
    individually.'''
    tagData, _, tagLabels = load_tags(os.path.join(imgDir, 'tag_file.csv'))
    tagLabels = [(tagLabels[i], i) for i in tagLabels.keys()]
    tagLabels.sort()
    tagLabels = [j for i,j in tagLabels]

    fileList = os.listdir(imgDir)

    #Identifies filenames that contain "msk"
    mskFiles = np.sort([iFile for iFile in fileList if 'msk' in iFile])

    for i_msk in range(len(mskFiles)):
      mskName = mskFiles[i_msk]
      imgName = mskName.replace('msk', 'img')
      
      img = imageio.imread(os.path.join(imgDir, imgName)).T
      msk = imageio.imread(os.path.join(imgDir, mskName)).T
      msk = (msk >= np.max(msk))
      msk = binary_fill_holes(msk).astype(int)

      rawImage = RawImageObject(img, msk, tagLabels)

      #Append tag data to image objects
      tags = tagData[np.where(tagData[:,1]==(i_msk+1))]
      tags = tags[:,[0,2,3]]
      rawImage.labelMask(tags)
      rawImage.enumerateMask()

      #Crop cell objects
      rawImage.cropImage(80)

      self.appendCropCollection(rawImage.getCropCollection())

    self.setLabels()
  
  def appendCropCollection(self, cropCollection):
    self.cropList.extend(cropCollection)

  def getBgCount(self):
    return len(self.bgList)

  def getCropCount(self):
    return len(self.cropList)

  def setLabels(self):
    labelDict = {}
    for i in self.cropList:
      if i.label not in labelDict:
        labelDict[i.label] = i.tag
    self.labels = labelDict

  def getLabels(self):
    return self.labels

  def adjustLabels(self, customLabels):
    '''Merges and/or extracts labels according to the information passed in the
    customLabels dictionary. This dictionary should have the following structure:
    {newLabel1 : [oldLabel1, oldLabel2], newLabel2 : [oldLabel3]}.
    New labels are numbered automatically and modified into all cropObject
    stored in cropList. Existing labels that are not included in the new
    dictionary are automatically discarded and crops belonging to this class
    deleted.'''

    currentLabels = self.labels
    newLabels = {}
    tag = 1
    for i in customLabels:
      newLabels[i] = tag
      tag +=1

    tagArray = [0]*len(currentLabels)
    for i in customLabels:
      for j in customLabels[i]:
          tagArray[currentLabels[j]-1] = newLabels[i]

    labelKeys = list(newLabels.keys())
    labelVals = list(newLabels.values())
    
    toDelete = []
    for i in range(len(self.cropList)):
      self.cropList[i].tag = tagArray[self.cropList[i].tag-1]
      if self.cropList[i].tag:
        self.cropList[i].label = labelKeys[labelVals.index(self.cropList[i].tag)]
      else:
        toDelete.append(i)

    # Remove zeroed crop objects from set
    toDelete.sort(reverse=True)
    for i in toDelete:
      self.cropList.pop(i)

    self.setLabels()

  def makeImages(self, n, imgSize, maxObjects, fractionEmpty, validationSize=0, testSize=.2):
    self.imgSize = imgSize

    del self.imageObjects; self.imageObjects = [[] for i in range(3)]

    trainSize = 1 - (validationSize + testSize)
    
    nClasses = len(self.labels)

    #Get indices of crops and separate into tag classes, and shuffle
    tagIdx = [[] for i in range(nClasses)]
    for i in range(len(self.cropList)):
      tagIdx[self.cropList[i].tag-1].append(i)
    [rd.shuffle(i) for i in tagIdx]

    #Calculate index ranges for training/validation/test sets
    classRanges = [None]*nClasses
    for i in range(nClasses):
      classObjectCount = len(tagIdx[i])
      validationStart = int(classObjectCount * trainSize)
      testStart = int(classObjectCount * validationSize + validationStart)
      classRanges[i] = [0, validationStart, testStart, classObjectCount]

    setSizes = [int(n * trainSize), int(n * validationSize), int(n * testSize)]

    n_fractions = n//10
    imageCount = 0
    setNames = ['Training', 'Validation', 'Test']
    for i in range(3):
      print(setNames[i]+" images:")
      setIdx = []
      for j in range(nClasses):
        setIdx.append(tagIdx[j][classRanges[j][i]:classRanges[j][i+1]])

      j = -1
      for j in range(setSizes[i]):
        imgObject = ImageObject(self.bgList, self.imgSize)
        imgObject.addObjectsToImage(self.cropList, maxObjects, fractionEmpty, idx=setIdx)
        imgObject.makeWeightMap()
        self.appendToSet(imgObject, i)
        imageCount += 1

        if (j+1) % n_fractions == 0 and j+1<n:
          print(j+1)
      if (j+1) % n_fractions:
        print(j+1, '\n')

    print("Total:", n)

    self.convertForNetwork()

  def convertForNetwork(self):
    self.trainingSet = self.getSetData('training')
    self.validationSet = self.getSetData('validation')
    self.testSet = self.getSetData('test')

    self.setOutputShape()
    self.setInputShape()

    del self.imageObjects; self.imageObjects = [[] for i in range(3)]

  def setInputShape(self):
    #self.inputShape = self.trainingSet.X.shape[1:]
    self.inputShape = (self.imgSize, self.imgSize, 1)

  def setOutputShape(self):
    #self.outputShape = self.trainingSet.y.shape[1:]
    self.outputShape = (self.imgSize, self.imgSize, len(self.labels))

  def appendToSet(self, imageObject, setType):
    self.imageObjects[setType].append(imageObject)

  def getSetData(self, setType):
    #Get images, masks, and weight maps
    X = []
    ss = []
    y = []
    w = []
    bb = []

    if setType == 'training':
      setID = 1
    elif setType == 'validation':
      setID = 2
    elif setType == 'test':
      setID = 3
    else:
      setID = 0
    
    if not setID or not self.imageObjects[setID-1]:
      X = np.array(X)
      y = np.array(y)
      w = np.array(w)

    else:
      for i in self.imageObjects[setID-1]:
        X.append(i.img)
        y.append(i.mask)
        w.append(i.weightMap)
        bb.append(i.bboxes)

      ## Reshape and normalize images
      X = np.asarray(X)
      X = (X - np.mean(X)) / np.std(X)
      X = X.reshape(X.shape + (1,))

      ss = np.asarray(y)
      ss = (ss>0)
      ss = ss.reshape(ss.shape + (1,))

      ## Separate masks into channels by label
      yTags = list(self.labels.values())
      yTags.sort()
      y = np.asarray(y)
      ystack = np.zeros(y.shape + (len(yTags),))

      for i in yTags:
        ystack[:,:,:,i-1][y==i] = 1
      y = np.copy(ystack)

      ## Reshape weight maps
      w = np.asarray(w)
      w = w.reshape(w.shape + (1,))

      return NetworkSet(X, ss, y, w, bb)

  def getSetCounts(self):
    if self.trainingSet:
      print("Training images:", self.trainingSet.getSize())
    if self.validationSet:
      print("Validation images:", self.validationSet.getSize())
    if self.testSet:
      print("Test images:", self.testSet.getSize())

  def newModel(self, classificationType='direct'):
    del self.model
    if classificationType=='direct':
      self.inputShape = self.inputShape[:-1] + (1,)
    elif classificationType=='assisted':
      self.inputShape = self.inputShape[:-1] + (2,)
    print(self.inputShape, self.outputShape)
    self.model = unet(input_size = self.inputShape, n_classes = self.outputShape[-1])

  def trainModel(self, epochs, batchSize=30):
    X = self.trainingSet.X
    ss = self.trainingSet.ss
    y = np.concatenate((self.trainingSet.w, self.trainingSet.y), axis=-1)

    if self.validationSet:
      X_val = self.validationSet.X
      ss_val = self.validationSet.ss
      y_val = np.concatenate((self.validationSet.w, self.validationSet.y), axis=-1)
    elif self.testSet:
      X_val = self.testSet.X
      ss_val = self.testSet.ss
      y_val = np.concatenate((self.testSet.w, self.testSet.y), axis=-1)
    else:
      X_val = np.array([])
      ss_val = np.array([])
      y_val = np.array([])
    
    if self.inputShape[-1]==2:
      X = np.concatenate((X,ss),axis=-1)
      X_val = np.concatenate((X_val,ss_val),axis=-1)

    try:
      if X_val.size:
        self.model.fit(x=X, y=y, epochs=epochs, batch_size=batchSize, validation_data=(X_val, y_val), verbose=1)
      else:
        self.model.fit(x=X, y=y, epochs=epochs, batch_size=batchSize, verbose=1)
    except AttributeError:
      print("No model to train.")

  def evaluateModel(self, batchSize=10):
    try:
      X = self.testSet.X
      if self.inputShape[-1]==2:
        ss = self.testSet.ss
        X = np.concatenate((X,ss),axis=-1)
      y = np.concatenate((self.testSet.w, self.testSet.y), axis=3)

      try:
        metrics = self.model.evaluate(x=X, y=y, batch_size=batchSize, verbose=1)
        print("Loss:",metrics[0],"\nDice index:",metrics[1])
      except AttributeError:
        print("No model to evaluate.")
    except AttributeError:
      print("No test set found.")

  def predictTestSet(self, batchSize=1):
    try:
      X = self.testSet.X
      if self.inputShape[-1]==2:
        ss = self.testSet.ss
        X = np.concatenate((X,ss),axis=-1)

      try:
        self.predictions = self.model.predict(X, batch_size=batchSize, verbose=1)
      except AttributeError:
        print("No model to use for predicting.")
    except AttributeError:
      print("No test set found.")

  def saveModel(self, save_dir, model_name):
    model_path = os.path.join(save_dir, model_name)
    pathlib.Path(save_dir).mkdir(exist_ok=True) 
    try:
      self.model.save(model_path)
      print(time.strftime("%Y-%m-%d, %H:%M:"), "Model saved to", model_path)
    except AttributeError:
      print("No model to save.")

  def loadModel(self, model_dir, model_name):
    model_path = os.path.join(model_dir, model_name)
    loadedModel = load_model(model_path, custom_objects={'weighted_dice_loss': weighted_dice_loss,
                                                         'dice_loss': dice_loss,
                                                         'dice_index': dice_index})  
    
    if self.outputShape[-1] != loadedModel._layers[-1].output_shape[-1]:
      print("Loaded model and image set outputs do not match. Model outputs",
            loadedModel._layers[-1].output_shape[-1],"classes, whereas image set",
            "only represents",self.outputShape[-1],"classes.")
      print("Model not loaded.")
    else:
      self.model = loadedModel
      if self.inputShape != self.model._layers[0].batch_input_shape[1:]:
        self.adjustModelInputShape()
      print("Model loaded successfully.")

  def adjustModelInputShape(self):
    #https://stackoverflow.com/questions/42187425/how-to-change-input-shape-in-sequential-model-in-keras
    oldInputShape = self.model._layers[0].batch_input_shape

    n_channels = oldInputShape[-1]
    if self.inputShape[-1] != n_channels:
      self.inputShape = self.inputShape[:-1] + (n_channels, )

    if self.inputShape != oldInputShape[1:]:
      # replace input shape of first layer
      newInputShape = (None, ) + self.inputShape
      self.model._layers[0].batch_input_shape = newInputShape

      # rebuild model architecture by exporting and importing via json
      new_model = model_from_json(self.model.to_json())

      new_model.compile(optimizer = 'adadelta', loss=weighted_dice_loss, metrics=[dice_index])

      # copy weights from old model to new one
      for layer in new_model.layers:
        try:
          layer.set_weights(self.model.get_layer(name=layer.name).get_weights())
          #print("Loaded layer {}".format(layer.name))
        except:
          print("Could not transfer weights for layer {}".format(layer.name))

      #self.model.summary()

      self.model = new_model

      print("Adjusted model input shape from",oldInputShape,"to",newInputShape)

  def saveSet(self, save_dir, filename):    
    model = None
    if self.model:
      model = self.model
      self.model = None
    pathlib.Path(save_dir).mkdir(exist_ok=True) 
    with open(os.path.join(save_dir, filename), 'wb') as f:
      pickle.dump(self, f)
    if model:
      self.model = model
    print(time.strftime("%Y-%m-%d, %H:%M:"), "Image set saved to", save_dir)
  
  def showImages(self):  
    #Define image range to display
    if self.inputShape[0]>200 or self.inputShape[1]>200:
      subRegion = True
    else:
      subRegion = False
      extent = [0, self.inputShape[0], 0, self.inputShape[1]]

    #Get class names sorted by class number
    classLabels = [(self.labels[i],i) for i in self.labels.keys()]
    classLabels.sort()

    vmax = len(classLabels)

    subplot_nrows = 4
    subplot_ncols = vmax+3
    subplot_width = 18

    plt.figure(figsize=(subplot_width, int(subplot_width/subplot_ncols*subplot_nrows)))
    for i_row in range(0, subplot_nrows):
      if subRegion:
        xRange = [max(rd.randint(0,self.inputShape[0])-200,0)]*2
        xRange[1] += min(self.inputShape[0],200)
        yRange = [max(rd.randint(0,self.inputShape[1])-200,0)]*2
        yRange[1] += min(self.inputShape[1],200)
        extent = [xRange[0], xRange[1], yRange[0], yRange[1]]
        
      i = rd.randint(0,self.trainingSet.X.shape[0]-1)
      plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+1)
      plt.grid(False)
      plt.imshow(self.trainingSet.X[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
      plt.imshow(self.trainingSet.ss[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='jet', interpolation='none', origin='lower', alpha=.4, extent=extent)
      for i_bbox in range(len(self.trainingSet.bb[i])):
        plt.gca().add_patch(plt.Rectangle((self.trainingSet.bb[i][i_bbox][0:2]), self.trainingSet.bb[i][i_bbox][2], self.trainingSet.bb[i][i_bbox][3], ec='r', fc='none'))
      plt.annotate('#{}'.format(i+1), (extent[0]+5,extent[3]-18), color='w')
      #plt.annotate(self.predictedImageNames[i], (xRange[0]+5,yRange[1]-18), color='w')
      if i_row==0:
          plt.title('Input')

      plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+2)
      plt.grid(False)
      plt.imshow(self.trainingSet.X[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
      plt.grid(False)
      plt.imshow(y_argmax(self.trainingSet.y[i])[extent[0]:extent[1],extent[2]:extent[3]].T, cmap='jet', vmin=0, vmax=vmax, interpolation='none', origin='lower', alpha = .5, extent=extent)
      if i_row==0:
          plt.title('Ground truth')

      plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+3)
      plt.grid(False)
      plt.imshow(self.trainingSet.X[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
      plt.grid(False)
      plt.imshow(self.trainingSet.w[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='jet', vmin=0, vmax=10, interpolation='none', origin='lower', extent=extent, alpha = .5)
      if i_row==0:
        plt.title('Weight Map')

      for i_class in range(vmax):
          plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+4+i_class)
          plt.grid(False)
          plt.imshow(self.trainingSet.y[i,extent[0]:extent[1],extent[2]:extent[3],i_class].T, vmin=0, vmax=1, cmap='jet', interpolation='none', origin='lower', extent=extent)
          if i_row==0:
              plt.title('Class \''+classLabels[i_class][1]+'\'')

  def showPredictions(self):  
    #Define image range to display
    if self.inputShape[0]>200 or self.inputShape[1]>200:
      subRegion = True
    else:
      subRegion = False
      extent = [0, self.inputShape[0], 0, self.inputShape[1]]

    #Get class names sorted by class number
    classLabels = [(self.labels[i],i) for i in self.labels.keys()]
    classLabels.sort()

    vmax = len(classLabels)

    subplot_nrows = 4
    subplot_ncols = vmax+3
    subplot_width = 18

    plt.figure(figsize=(subplot_width, int(subplot_width/subplot_ncols*subplot_nrows)))
    for i_row in range(0, subplot_nrows):
      if subRegion:
        xRange = [max(rd.randint(0,self.inputShape[0])-200,0)]*2
        xRange[1] += min(self.inputShape[0],200)
        yRange = [max(rd.randint(0,self.inputShape[1])-200,0)]*2
        yRange[1] += min(self.inputShape[1],200)
        extent = [xRange[0], xRange[1], yRange[0], yRange[1]]
        
      i = rd.randint(0,self.testSet.X.shape[0]-1)
      plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+1)
      plt.grid(False)
      plt.imshow(self.testSet.X[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
      if self.inputShape[-1]==2:
        plt.imshow(self.testSet.ss[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='jet', interpolation='none', origin='lower', alpha=.4, extent=extent)
      for i_bbox in range(len(self.testSet.bb[i])):
        plt.gca().add_patch(plt.Rectangle((self.testSet.bb[i][i_bbox][0:2]), self.testSet.bb[i][i_bbox][2], self.testSet.bb[i][i_bbox][3], ec='r', fc='none'))
      plt.annotate('#{}'.format(i+1), (extent[0]+5,extent[3]-18), color='w')
      #plt.annotate(self.predictedImageNames[i], (xRange[0]+5,yRange[1]-18), color='w')
      if i_row==0:
          plt.title('Input')

      plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+2)
      plt.grid(False)
      plt.imshow(self.testSet.X[i,extent[0]:extent[1],extent[2]:extent[3],0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
      plt.grid(False)
      plt.imshow(y_argmax(self.testSet.y[i])[extent[0]:extent[1],extent[2]:extent[3]].T, cmap='jet', vmin=0, vmax=vmax, interpolation='none', origin='lower', alpha = .5, extent=extent)
      if i_row==0:
          plt.title('Ground truth')

      plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+3)
      plt.grid(False)
      plt.imshow(self.testSet.X[i,:,:,0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
      plt.grid(False)
      plt.imshow(y_argmax(self.predictions[i])[extent[0]:extent[1],extent[2]:extent[3]].T, cmap='jet', vmin=0, vmax=vmax, interpolation='none', origin='lower', extent=extent, alpha = .5)
      if i_row==0:
        plt.title('Prediction')

      for i_class in range(vmax):
          plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+4+i_class)
          plt.grid(False)
          plt.imshow(self.predictions[i,extent[0]:extent[1],extent[2]:extent[3],i_class].T, vmin=0, vmax=1, cmap='jet', interpolation='none', origin='lower', extent=extent)
          if i_row==0:
              plt.title('Class \''+classLabels[i_class][1]+'\'')


class NetworkSet:
  def __init__(self, X, ss, y, w, bb):
    self.X = X
    self.ss = ss
    self.y = y
    self.w = w
    self.bb = bb

  def getSize(self):
    return self.X.shape[0]


class PredictionJob:
  def __init__(self, imgDir):
    self.imgDir = imgDir
    self.inputShape = None
    self.cachedImages = []
    self.cachedImageNames = []
    self.predictedImageNames = None
    self.predictions = None
    self.processedImageNames = []

    fileList = os.listdir(imgDir)

    self.queuedImageNames = [i for i in fileList if 'img' in i]
    self.queuedImageNames.sort()

    self.setInputShape()

  def setInputShape(self):
    imgShape = imageio.imread(os.path.join(self.imgDir, self.queuedImageNames[0])).T.shape
    self.inputShape = imgShape

  def fillImageCache(self):
    #Fills image cache to 50 images not previously processed
    loadCount = len(self.cachedImages)
    while loadCount<50:
      try:
        i_img = self.queuedImageNames[0]
        img = imageio.imread(os.path.join(self.imgDir, i_img)).T
        img = (img - np.mean(img)) / np.std(img)
        img = img.reshape(img.shape + (1,))

        if self.classificationType=="assisted":
          try:
            i_msk = i_img.replace('img','msk')
            msk = imageio.imread(os.path.join(self.imgDir, i_msk)).T
            msk = (msk >= np.max(msk))
            msk = binary_fill_holes(msk).astype(int)
            msk = msk.reshape(msk.shape + (1,))
            img = np.concatenate((img, msk), axis=-1)
            
          except FileNotFoundError:
            print("Could not find image mask for assisted classification.")
            return False
        
        self.queuedImageNames.pop(0)
        self.cachedImageNames.append(i_img)
        self.cachedImages.append(img)

        loadCount += 1
      except IndexError:
        loadCount = 50
    return True

  def loadModel(self, model_dir, model_name):
    model_path = os.path.join(model_dir, model_name)
    self.model = load_model(model_path, custom_objects={'weighted_dice_loss': weighted_dice_loss,
                                                        'dice_loss': dice_loss,
                                                        'dice_index': dice_index})
    self.adjustModelInputShape()

    print("Model loaded successfully")

  def adjustModelInputShape(self):
    #https://stackoverflow.com/questions/42187425/how-to-change-input-shape-in-sequential-model-in-keras
    oldInputShape = self.model._layers[0].batch_input_shape

    n_channels = oldInputShape[-1]
    if n_channels == 1:
      self.classificationType = "direct"
    else:
      self.classificationType = "assisted"

    # replace input shape of first layer
    newInputShape = (None, ) + self.inputShape + (n_channels, )
    self.model._layers[0].batch_input_shape = newInputShape

    # rebuild model architecture by exporting and importing via json
    new_model = model_from_json(self.model.to_json())

    # copy weights from old model to new
    for layer in new_model.layers:
      try:
        layer.set_weights(self.model.get_layer(name=layer.name).get_weights())
        #print("Loaded layer {}".format(layer.name))
      except:
        print("Could not transfer weights for layer {}".format(layer.name))

    #self.model.summary()

    self.model = new_model

    print("Adjusted model input shape from",oldInputShape,"to",newInputShape)

  def predict(self):
    if self.predictedImageNames:
      del self.predictions; self.predictions = None; del self.predictedImages
      self.queuedImageNames.extend(self.predictedImageNames)
      self.predictedImageNames = None
    
    if self.fillImageCache():
      self.predictedImages = np.array(self.cachedImages)

      #Predict on currently cached images, and mark images as processed
      self.predictions = self.model.predict(self.predictedImages, batch_size=1, verbose=1)
      del self.cachedImages; self.cachedImages = []
      self.predictedImageNames = self.cachedImageNames
      self.cachedImageNames = []

  def predictAndOutput(self, outDir, normalize=True):
    self.fillImageCache()
    while self.cachedImages:
      #Predict on currently cached images, and mark images as processed
      self.predictedImages = np.array(self.cachedImages)
      self.predictions = self.model.predict(self.predictedImages, batch_size=1, verbose=1)
      del self.cachedImages; self.cachedImages = []
      self.predictedImageNames = self.cachedImageNames
      self.cachedImageNames = []

      #Output predictions
      self.outputPredictions(outDir, normalize=True)

      #Load new images
      self.fillImageCache()
  
  def outputPredictions(self, out_dir, normalize=True):
    try:
      if normalize:
        classValues = list(range(self.predictions.shape[-1]+1))
        classValues.pop(0)
        normValues = []
        for i in classValues:
          normValues.append(255//i)
        normValues.sort()
        #print(classValues,normValues)

      pathlib.Path(out_dir).mkdir(exist_ok=True)

      filenames = []
      for i_out in range(len(self.predictedImageNames)):
        filename = self.predictedImageNames[i_out].replace("img","prd")

        #prd = self.predictions[i_out]*(self.predictions[i_out]>.8)
        #prd = y_argmax(prd).T
        prd = y_argmax(self.predictions[i_out]).T
          
        if normalize:
          for i_val in range(len(classValues),0,-1):
            prd[prd==classValues[i_val-1]] = normValues[i_val-1]

        prd = pil.Image.fromarray(prd.astype(np.uint8))
        prd.save(os.path.join(out_dir, filename))
        filenames.append(filename)
      print("Output:", filenames,'\n')
      self.processedImageNames.extend(self.predictedImageNames)
      self.predictedImageNames = []

      del self.predictions; self.predictions = None; del self.predictedImages
    except TypeError:
      print("No predictions to output.")

  def getClassCount(self):
    return self.model._layers[-1].output_shape[-1]

  def showPredictions(self):
    #Get class names sorted by class number
    classCount = self.getClassCount()

    #Get class names sorted by class number
    try:
      classLabels = [(setLabels[i],i) for i in setLabels.keys()]
      classLabels.sort()
    except:
      classLabels = [(i+1,str(i+1)) for i in range(classCount)]

    vmax = classCount

    subplot_nrows = 4
    subplot_ncols = classCount+2
    subplot_width = 18

    plt.figure(figsize=(subplot_width, int(subplot_width/subplot_ncols*subplot_nrows)))
    for i_row in range(0, subplot_nrows):
        xRange = [rd.randint(0,self.predictedImages.shape[1]-200)]*2
        xRange[1] += 200
        yRange = [rd.randint(0,self.predictedImages.shape[2]-200)]*2
        yRange[1] += 200
        extent = [xRange[0], xRange[1], yRange[0], yRange[1]]
        
        i = rd.randint(0,self.predictedImages.shape[0]-1)
        plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+1)
        plt.grid(False)
        plt.imshow(self.predictedImages[i,xRange[0]:xRange[1],yRange[0]:yRange[1],0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
        if self.predictedImages.shape[3] > 1:
          plt.imshow(self.predictedImages[i,xRange[0]:xRange[1],yRange[0]:yRange[1],1].T, cmap='jet', interpolation='none', origin='lower', alpha=.4, extent=extent)
        #plt.annotate('#{}'.format(i+1), (xRange[0]+5,yRange[1]-18), color='w')
        plt.annotate(self.predictedImageNames[i], (xRange[0]+5,yRange[1]-18), color='w')
        if i_row==0:
            plt.title('Input')

        plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+2)
        plt.grid(False)
        plt.imshow(self.predictedImages[i,xRange[0]:xRange[1],yRange[0]:yRange[1],0].T, cmap='gray', interpolation='none', origin='lower', extent=extent)
        plt.grid(False)
        plt.imshow(y_argmax(self.predictions[i])[xRange[0]:xRange[1],yRange[0]:yRange[1]].T, cmap='jet', vmin=0, vmax=vmax, interpolation='none', origin='lower', alpha = .5, extent=extent)
        if i_row==0:
            plt.title('Prediction argmax')

        for i_class in range(self.predictions.shape[3]):
            plt.subplot(subplot_nrows, subplot_ncols, i_row*subplot_ncols+3+i_class)
            plt.grid(False)
            plt.imshow(self.predictions[i,xRange[0]:xRange[1],yRange[0]:yRange[1],i_class].T, vmin=0, vmax=1, cmap='jet', interpolation='none', origin='lower', extent=extent)
            if i_row==0:
                plt.title('Class \''+classLabels[i_class][1]+'\'')


def y_argmax(y):
  y_a = np.round(y)

  y_bg = np.zeros(y.shape[0:-1])
  y_sum = np.sum(y_a, axis=2)
  y_bg[np.where(y_sum==0)] = 1

  y_bg = y_bg.reshape(y_bg.shape + (1,))
  y_a = np.append(y_bg, y_a, axis=2)

  ##Make labeled masks
  y_a = np.argmax(y_a, axis=2)

  return y_a


def load_tags(filepath):
  ## Create empty list and import from CSV file
  data_table = []
  with open(filepath, newline='') as csvfile:
      cellid_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
      for row in cellid_reader:
          data_table.append(row)

  ## Extract header
  tag_header = data_table[0]
  for i_str in range(len(tag_header)):
    tag_header[i_str] = tag_header[i_str].replace("\"", "")

  ## Extract data
  data_idx = [tag_header.index(i) for i in tag_header if i!='Label']
  data_header = [tag_header[i] for i in data_idx]
  tag_data = np.array(data_table)[1:,data_idx].astype('int')

  ## Extract labels
  label_idx = [tag_header.index(i) for i in tag_header if i=='Label']
  type_idx = [tag_header.index(i) for i in tag_header if i=='Type'][0]

  label_data = np.array(data_table)[1:,label_idx]
  label_set = np.unique(label_data)
  
  tagLabels = {}
  for i in label_set:
    idx = np.nonzero(label_data == i)[0][0]
    tagLabels.update( {i : tag_data[idx, type_idx]} )
      
  return tag_data, data_header, tagLabels


def loadImageSet(set_path, filename):
  file_path = os.path.join(set_path, filename)
  try:
    with open(file_path, 'rb') as f:
      imageSet = pickle.load(f)

      imageSet.getSetCounts()
      print("Image set loaded successfully.")

      return imageSet
  except FileNotFoundError:
    print(file_path, "not found.")


def unet(pretrained_weights = None, input_size = None, n_classes = None):

  ksize = 3
  dpout = 0.5
  dpup = int(1/dpout)
  mpool = 2

  inputs = Input(input_size)
  conv1 = Conv2D(32, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
  conv1 = Conv2D(32, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
  pool1 = MaxPooling2D(pool_size=(mpool, mpool))(conv1)
  conv2 = Conv2D(64, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
  conv2 = Conv2D(64, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
  pool2 = MaxPooling2D(pool_size=(mpool, mpool))(conv2)
  conv3 = Conv2D(128, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
  conv3 = Conv2D(128, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
  pool3 = MaxPooling2D(pool_size=(mpool, mpool))(conv3)
  conv4 = Conv2D(256, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
  conv4 = Conv2D(256, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
  #drop4 = Dropout(dpout)(conv4)
  #pool4 = MaxPooling2D(pool_size=(mpool, mpool))(drop4)
  pool4 = MaxPooling2D(pool_size=(mpool, mpool))(conv4)

  conv5 = Conv2D(512, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
  conv5 = Conv2D(512, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
  #drop5 = Dropout(dpout)(conv5)

  up6 = Conv2D(256, dpup, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv5))
  merge6 = concatenate([conv4,up6], axis = 3)
  conv6 = Conv2D(256, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
  conv6 = Conv2D(256, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

  up7 = Conv2D(128, dpup, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv6))
  merge7 = concatenate([conv3,up7], axis = 3)
  conv7 = Conv2D(128, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
  conv7 = Conv2D(128, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

  up8 = Conv2D(64, dpup, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv7))
  merge8 = concatenate([conv2,up8], axis = 3)
  conv8 = Conv2D(64, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
  conv8 = Conv2D(64, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

  up9 = Conv2D(32, dpup, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv8))
  merge9 = concatenate([conv1,up9], axis = 3)
  conv9 = Conv2D(32, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
  conv9 = Conv2D(32, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
  conv9 = Conv2D(2, ksize, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
  conv10 = Conv2D(n_classes, 1, activation = 'sigmoid')(conv9)

  model = Model(inputs = inputs, outputs = conv10)

  model.compile(optimizer = 'adadelta', loss=weighted_dice_loss, metrics=[dice_index])
  
  #model.summary()

  if(pretrained_weights):
    model.load_weights(pretrained_weights)
  
  return model


def weighted_dice_loss(y_true, y_pred, ignore_background=False, square=True):
  #Extract weight map and true labels
  w_true = y_true[..., 0]
  w_true = tf.expand_dims(w_true, -1)
  y_true = y_true[..., 1:]

  ### Optional class err penalty ###
  #Calculate inverse class masks
  penalty = tf.constant(15, dtype=tf.float32)
  zero = tf.constant(0, dtype=tf.float32)
  bg_true = tf.cast(tf.equal(y_true, zero), tf.float32)
  #fg_true = tf.cast(tf.greater(y_true, zero), tf.float32)
  fg_true = tf.expand_dims(tf.reduce_sum(y_true, axis=-1), axis=-1)
  #err_penalty = y_pred * bg_true * penalty
  #w_true = bg_true * penalty + w_true
  w_true = bg_true * fg_true * penalty + w_true
  ###

  if ignore_background:
    y_true = y_true[..., 1:]
    y_pred = y_pred[..., 1:]

  axis = tuple(range(4))
  eps = 1e-7
  nom = 2 * tf.reduce_sum(y_true * y_pred, axis=axis)
  if square:
    y_true = tf.square(y_true)
    y_pred = tf.square(y_pred)

  #denom = tf.reduce_sum(y_true + y_pred, axis=axis) + eps #without weight map
  denom = tf.reduce_sum((y_true + y_pred) * w_true, axis=axis) + eps #with weight map

  return 1 - tf.reduce_mean(nom / denom)


def dice_loss(y_true, y_pred, ignore_background=False, square=True):
  #Keep true labels and discard weight map
  y_true = y_true[..., 1:]

  if ignore_background:
    y_true = y_true[..., 1:]
    y_pred = y_pred[..., 1:]
  
  axis = tuple(range(4))
  eps = 1e-7
  nom = 2 * tf.reduce_sum(y_true * y_pred, axis=axis)
  if square:
    y_true = tf.square(y_true)
    y_pred = tf.square(y_pred)

  denom = tf.reduce_sum(y_true + y_pred, axis=axis) + eps #without weight map

  return 1 - tf.reduce_mean(nom / denom)


def dice_index(y_true, y_pred):
  return 1 - dice_loss(y_true, y_pred)